﻿using UnityEngine;
using System.Collections;

public class HamsterAnimator : MonoBehaviour {

    public float rotationVelocity;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        gameObject.GetComponent<RectTransform>().Rotate(0f,0f,-Time.deltaTime * rotationVelocity);
	}
}
