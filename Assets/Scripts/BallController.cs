﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BallController : MonoBehaviour {

    public float speedingCoefficient;
    public float brakingCoefficient;

	// Use this for initialization
	void Start () {
        Time.timeScale = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKey("r"))
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        if(Input.GetKey("escape"))
            SceneManager.LoadScene(0);
        if(Input.GetKey("left") || Input.GetKey("right"))
            Time.timeScale = 1.0f;
    }

    void FixedUpdate () {
        if(Input.GetKey("left") && Input.GetKey("right")) {
            //Debug.Log("both");
        } else if(Input.GetKey("left")) {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2((1 - brakingCoefficient * Time.fixedDeltaTime) * gameObject.GetComponent<Rigidbody2D>().velocity.x,(1 - brakingCoefficient * Time.fixedDeltaTime) * gameObject.GetComponent<Rigidbody2D>().velocity.y);
            //Debug.Log("braking");
        } else if(Input.GetKey("right")) {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2((1 + speedingCoefficient * Time.fixedDeltaTime) * gameObject.GetComponent<Rigidbody2D>().velocity.x,(1 + speedingCoefficient * Time.fixedDeltaTime) * gameObject.GetComponent<Rigidbody2D>().velocity.y);
            //Debug.Log("speeding");
        }
    }

    void OnTriggerExit2D(Collider2D col) {
        if(col.gameObject.tag == "MainCamera") {
            //Debug.Log("exit main camera");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    void OnTriggerEnter2D(Collider2D col) {
        if(col.gameObject.tag == "Goal") {
            //Debug.Log("reached the goal");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    void OnCollisionEnter2D(Collision2D col) {
        if(col.gameObject.tag == "Bumper") {
            Debug.Log("contact with Bumper");
            GetComponent<AudioSource>().Play();
        }
    }
}
